# Package the SELinux policy
%bcond_without selinux
%global selinuxtype targeted

# Enable openvpn3-addon-aws package by default
%bcond_without aws

%bcond_without unittests

# EL-7 builds are now done using a newer compiler
# via the devtoolset approach; GCC 4.8.5 is aging
# a bit too much.
%define devtoolset devtoolset-10

# The DCO feature is only available on EL-8+ or Fedora
%if 0%{?rhel} > 7 || 0%{?fedora} > 31
%bcond_without dco
%else
%bcond_with dco
%endif

%global _hardened_build 1

# %%define versiontag _beta
# %%define releasetag .beta1

Name:           openvpn3
Version:        20
Release:        2%{?releasetag}%{?dist}
Summary:        TLS based VPN based on OpenVPN 3

License:        AGPLv3
URL:            https://github.com/OpenVPN/openvpn3-linux/
Source0:        https://swupdate.openvpn.net/community/releases/openvpn3-linux-%{version}%{?versiontag}.tar.xz
Source1:        https://swupdate.openvpn.net/community/releases/openvpn3-linux-%{version}%{?versiontag}.tar.xz.asc
Source2:        gpgkey-F554A3687412CFFEBDEFE0A312F5F7B42F2B01E7.gpg
Patch0:         fedora-crypto-policy-compliance.patch
Vendor:         OpenVPN Inc

# Currently the code is buildable on 32-bit architectures
ExcludeArch:    armv7hl i686

BuildRequires:  autoconf
BuildRequires:  autoconf-archive
BuildRequires:  automake
BuildRequires:  %{?el7:%devtoolset-}gcc-c++
BuildRequires:  glib2-devel
BuildRequires:  jsoncpp-devel
BuildRequires:  libcap-ng-devel
BuildRequires:  libuuid-devel
BuildRequires:  lz4-devel
BuildRequires:  systemd
BuildRequires:  systemd-devel
BuildRequires:  zlib-devel
BuildRequires:  openssl-devel

%if 0%{?rhel} == 7
BuildRequires:  python36-dbus
BuildRequires:  python36-devel
BuildRequires:  python36-jinja2
# We cannot use python36-docutils on RHEL-7 as
# the ./configure script does not currently find
# the rst2man-3 executable, it only looks for rst2man
BuildRequires:  python-docutils
%else
BuildRequires:  python3-dbus
BuildRequires:  python3-devel
BuildRequires:  python3-jinja2
BuildRequires:  python3-docutils
%endif

Requires:       openssl
Requires:       dbus
Requires:       jsoncpp
Requires:       lz4
Requires:       python(abi) >= 3.6
Requires:       python3-dbus
%if 0%{?rhel} == 7
Requires:       python36-gobject-base
%else
Requires:       python3-gobject-base
Requires:       python3-systemd
Requires:       polkit
%endif

# DCO support dependencies
%if %{with dco}
BuildRequires:  libnl3-devel
BuildRequires:  protobuf-compiler
BuildRequires:  protobuf-devel
Requires:       libnl3
Requires:       protobuf

%if 0%{?rhel} > 7
Recommends:     kmod-ovpn-dco < 0.2
%endif

%endif
# End - DCO support deps

%if %{with selinux}
Requires:       %{name}-selinux >= %{version}-%{release}
%endif


%description
OpenVPN 3 based implementation targeting modern Linux distributions.
OpenVPN 3 aims to be protocol compatible with the older OpenVPN 2.x
releases, but does not support all features of OpenVPN 2.x.

%package devel
Summary:       Development headers for OpenVPN 3 Linux

%description devel
Contains generated C header file needed to have correct mapping of
constants used by OpenVPN 3 Linux.


%package client
Summary:       OpenVPN 3 Client, TLS based VPN client
Requires:      %{name}%{?_isa} = %{version}-%{release}

%description client
OpenVPN 3 Client components.  Provides the binaries and D-Bus services
required to initiate and manage VPN client configuration profiles and
manage VPN client sessions.

# openvpn3-addon-aws sub-package
%if %{with aws}
%package addon-aws
Summary:       OpenVPN 3 Linux AWS VPC integration support
BuildRequires: tinyxml2-devel
Requires:      tinyxml2

%description addon-aws
This OpenVPN 3 Linux add-on will push VPN routes to the
AWS VPC to enable hosts inside the related VPC to utilize
the VPN setup.
%endif

# SELinux sub-package
%if %{with selinux}
%package selinux
Summary:        OpenVPN 3 Linux SELinux policy
BuildArch:      noarch
BuildRequires:  selinux-policy-devel
Requires:       selinux-policy-%{selinuxtype}
%{?selinux_requires}

%description selinux
Additional SELinux policy required for OpenVPN 3 Linux to function
when SELinux is active.
%endif


%prep
gpgv2 --quiet --keyring %{SOURCE2} %{SOURCE1} %{SOURCE0}
%setup -q -n %{name}-linux-%{version}%{?versiontag}
%if 0%{?rhel} > 8 || 0%{?fedora} > 31
%patch0 -p1
%endif

%build
%if 0%{?fedora} > 34
# This is a workaround for the time being
# as ./configure will have python detection code
# stripping Python 3.10 to 3.1
# Additionally, out-of-the-box `Makefile` would use `import imp`
#   during `make install` that's deprecated in Python-3.4 and
#   removed in Python-3.12 (used in Fedora-39)
autoreconf -fi -v
%endif

# Configure and build
%{?el7:. /opt/rh/%devtoolset/enable}
%configure CXXFLAGS="%{?rhel7_optflags} %{optflags}" SYSTEMD_UNIT_DIR="%{_unitdir}" --disable-build-test-progs %{?!with_unittests:--disable-unit-tests} --enable-bash-completion %{?with_aws:--enable-addons-aws} %{!?with_selinux:--disable-selinux-build} %{?with_dco:--enable-dco}
make %{?_smp_mflags}

%check
%if %{with unittests}
src/tests/unit/unit-tests --gtest_output="xml:test_results/openvpn3-linux-tests.xml"
%endif
make check
src/ovpn3cli/openvpn3 version
src/ovpn3cli/openvpn3-admin version

%install
rm -rf $RPM_BUILD_ROOT
%make_install
mkdir -p %{buildroot}%{_sysconfdir}/%{name}/autoload
mkdir -p %{buildroot}%{_sharedstatedir}/%{name}/configs

%pre
# Ensure we have openvpn user and group accounts
getent group openvpn &>/dev/null || groupadd -r openvpn
getent passwd openvpn &>/dev/null || \
    /usr/sbin/useradd -r -g openvpn -s /sbin/nologin -c OpenVPN \
        -d %{_sharedstatedir}/%{name} openvpn
exit 0

%preun
#
#  SELinux sub-package
#
%if %{with selinux}
%post selinux
# Install SELinux policy
%selinux_modules_install -s %{selinuxtype} %{_datadir}/selinux/packages/%{name}.pp.bz2
%selinux_modules_install -s %{selinuxtype} %{_datadir}/selinux/packages/%{name}_service.pp.bz2

# Enable the dbus_access_tuntap_device SELinux boolean.
# This is needed to make it possbile for the netcfg service
# to pass the file descriptor to tun devices it has created
# and configured.
%selinux_set_booleans -s %{selinuxtype} dbus_access_tuntap_device=1

%postun selinux
# Unset dbus_access_tuntap_device SELinux boolean
# and uninstall the SELinux policy we provide
%selinux_unset_booleans -s %{selinuxtype} dbus_access_tuntap_device=1
%selinux_modules_uninstall -s %{selinuxtype} %{_datadir}/selinux/packages/%{name}.pp.bz2
%selinux_modules_uninstall -s %{selinuxtype} %{_datadir}/selinux/packages/%{name}_service.pp.bz2

%endif


%files
%doc COPYRIGHT.md README.md QUICK-START.md docs/*.md docs/dbus/*.md
%config(noreplace) %{_datarootdir}/dbus-1/system.d/net.openvpn.v3.conf
%config(noreplace) %{_datarootdir}/dbus-1/system.d/net.openvpn.v3.configuration.conf
%config(noreplace) %{_datarootdir}/dbus-1/system.d/net.openvpn.v3.log.conf
%config(noreplace) %{_datarootdir}/dbus-1/system.d/net.openvpn.v3.netcfg.conf
%config %dir %{_sysconfdir}/%{name}
%config %dir %{_sysconfdir}/%{name}/autoload
%{_libexecdir}/openvpn3-linux/openvpn3-service-logger
%{_libexecdir}/openvpn3-linux/openvpn3-service-configmgr
%{_libexecdir}/openvpn3-linux/openvpn3-service-netcfg
%{_unitdir}/openvpn3-autoload.service
%{_bindir}/openvpn3
%{_sbindir}/openvpn3-autoload
%{_sbindir}/openvpn3-admin
%{python3_sitelib}/openvpn3/*.py
%{python3_sitelib}/openvpn3/__pycache__/*
%{_datarootdir}/bash-completion/completions/openvpn*
%{_datarootdir}/dbus-1/system-services/net.openvpn.v3.configuration.service
%{_datarootdir}/dbus-1/system-services/net.openvpn.v3.log.service
%{_datarootdir}/dbus-1/system-services/net.openvpn.v3.netcfg.service
%{_datarootdir}/polkit-1/rules.d/net.openvpn.v3.rules
%dir %attr(750, openvpn, openvpn)%{_sharedstatedir}/%{name}
%dir %attr(750, openvpn, openvpn)%{_sharedstatedir}/%{name}/configs
%{_mandir}/man7/openvpn3-linux.7*
%{_mandir}/man1/openvpn3.1*
%{_mandir}/man1/openvpn3-config*.1*
%{_mandir}/man1/openvpn3-log.1*
%{_mandir}/man1/openvpn3-session*.1*
%{_mandir}/man8/openvpn3-admin*.8*
%{_mandir}/man8/openvpn3-autoload.8*
%{_mandir}/man8/openvpn3-service-logger.8*
%{_mandir}/man8/openvpn3-service-configmgr.8*
%{_mandir}/man8/openvpn3-service-netcfg.8*


%files devel
%doc COPYRIGHT.md
%{_includedir}/openvpn3

%files client
%config(noreplace) %{_datarootdir}/dbus-1/system.d/net.openvpn.v3.client.conf
%config(noreplace) %{_datarootdir}/dbus-1/system.d/net.openvpn.v3.sessions.conf
%{_bindir}/openvpn2
%{_bindir}/openvpn3-as
%{_libexecdir}/openvpn3-linux/openvpn3-service-backendstart
%{_libexecdir}/openvpn3-linux/openvpn3-service-client
%{_libexecdir}/openvpn3-linux/openvpn3-service-sessionmgr
%{_libexecdir}/openvpn3-linux/openvpn3-systemd
%{_datarootdir}/dbus-1/system-services/net.openvpn.v3.backends.service
%{_datarootdir}/dbus-1/system-services/net.openvpn.v3.sessions.service
%{_unitdir}/openvpn3-session@.service
%{_mandir}/man1/openvpn2.1*
%{_mandir}/man1/openvpn3-as.1*
%{_mandir}/man8/openvpn3-service-backendstart.8*
%{_mandir}/man8/openvpn3-service-client.8*
%{_mandir}/man8/openvpn3-service-sessionmgr.8*
%{_mandir}/man8/openvpn3-systemd.8*

%if %{with aws}
%files addon-aws
%config(noreplace) %{_datarootdir}/dbus-1/system.d/net.openvpn.v3.aws.conf
%{_libexecdir}/openvpn3-linux/openvpn3-service-aws
%{_unitdir}/openvpn3-aws.service
%{_mandir}/man8/openvpn3-service-aws.8*
%{_sysconfdir}/%{name}/awscerts
%endif

%if %{with selinux}
%files selinux
%{_datadir}/selinux/packages/%{name}.pp.*
%{_datadir}/selinux/packages/%{name}_service.pp.*
%ghost %{_sharedstatedir}/selinux/%{selinuxtype}/active/modules/200/%{name}
%ghost %{_sharedstatedir}/selinux/%{selinuxtype}/active/modules/200/%{name}_service
%endif


%changelog
* Mon Sep 11 2023 ojab <ojab@ojab.ru> - 20-3
- Packaging fix only - regenerate Makefile for Python-3.12 compatibility

* Fri Apr 21 2023 David Sommerseth <davids@openvpn.net> - 20-2
- Packaging fix only - Recommend kmod-ovpn-dco < 0.2
  - openvpn3 v20 is not ready for newer kmod-ovpn-dco versions yet
  - Improved descriptions

* Mon Mar 20 2023 David Sommerseth <davids@openvpn.net> - 20-1
- Package the openvpn3-linux-20 stable release

* Fri Oct 28 2022 David Sommerseth <davids@openvpn.net> - 19-1.beta
- Package the openvpn3-linux-19_beta release

* Tue Jun 7 2022 David Sommerseth <davids@openvpn.net> - 18-1.beta
- Package the openvpn3-linux-18_beta release
- D-Bus policies has been relocated to %%{_datarootdir}/dbus-1/system.d

* Mon Dec 13 2021 David Sommerseth <davids@openvpn.net> - 17-2.beta1
- Package the openvpn3-linux-17_beta release
- Build EPEL-7 using devtoolset-10

* Tue Oct 19 2021 David Sommerseth <davids@openvpn.net> - 16-1.beta1
- Package openvpn3-linux-16_beta release
- Include packaging of the new openvpn3-systemd integration

* Wed Jul 14 2021 David Sommerseth <davids@openvpn.net> - 15-1.beta1
- Package openvpn3-linux-15_beta release

* Wed Jul 14 2021 David Sommerseth <davids@openvpn.net> - 15-0.beta1
- Package openvpn3-linux-15_beta release

* Wed Jul 7 2021 David Sommerseth <davids@openvpn.net> - 14-0.beta1
- Package openvpn3-linux-14_beta release

* Sat Dec  5 2020 David Sommerseth <davids@openvpn.net> - 13-0.beta1
- Package openvpn3-linux-13_beta release

* Mon Nov 16 2020 David Sommerseth <davids@openvpn.net> - 12-0.beta1
- Package openvpn3-linux-12_beta release

* Fri Oct 30 2020 David Sommerseth <davids@openvpn.net> - 11-0.beta1
- Package openvpn3-linux-11_beta release
- Enable building with DCO support on Fedora and EL-8
- Ensure D-Bus policies are packaged in the proper sub-package, not all in the main package.
- Ensure openvpn3 man pages are in the proper sub-package

* Sat Jul 25 2020 David Sommerseth <davids@openvpn.net> - 10-0.beta1
- Package openvpn3-linux-10_beta release
- Move openvpn3 binary from -client to the base package
- Install bash-completions for openvpn2 in addition
- Install additional AWS region certificates in sysconfdir (openvpn3-addon-aws)
- Build RHEL-7 packages with -std=c++1y

* Tue Apr 28 2020 David Sommerseth <davids@openvpn.net> - 9-6.beta1
- Add explicit dependency on python3-gobject-base

* Tue Apr 28 2020 David Sommerseth <davids@openvpn.net> - 9-5.beta1
- Add explicit dependency on python3-dbus

* Tue Apr 28 2020 David Sommerseth <davids@openvpn.net> - 9-4.beta1
- Make use of the %%{selinux_requires} macro for SELinux dependency handling in the -selinux sub-package

* Tue Apr 28 2020 David Sommerseth <davids@openvpn.net> - 9-3.beta1
- Fix various openvpn3-selinux dependency related issues

* Sat Apr 25 2020 David Sommerseth <davids@openvpn.net> - 9-2.beta1
- Remove the Fedora packaging OpenSSL compliance patch on all distro releases older than Fedora 32

* Thu Apr 23 2020 David Sommerseth <davids@openvpn.net> - 9-1.beta1
- Packaging of the openvpn3-linux-9_beta release
- Reworked sub-packaging slightly, use proper bcond macros
- Added the new openvpn3-addon-aws sub-package

* Thu Feb 20 2020 David Sommerseth <davids@openvpn.net> - 8-2.beta1
- Package SELinux policy in a separate package

* Thu Feb 20 2020 David Sommerseth <davids@openvpn.net> - 8-1.beta1
- Adhere to Fedora Crypto Policy, using PROFILE=SYSTEM for cipher list init

* Mon Feb 10 2020 David Sommerseth <davids@openvpn.net> - 8-0.beta1
- Packaging of the openvpn3-linux-8_beta release
- Added additional compiler flags specific for RHEL-7

* Wed Dec 11 2019 David Sommerseth <davids@openvpn.net> - 7-0.beta1
- Packaging of the openvpn3-linux-7_beta release
- Corrected incorrect packaging of openvpn3-autoload.service file

* Fri May 24 2019 David Sommerseth <davids@openvpn.net> - 6-0.beta1
- Packaging of the openvpn3-linux-6_beta release
- This moves over to OpenSSL 1.1 on distributions providing that

* Wed Apr 3 2019 David Sommerseth <davids@openvpn.net> - 5-0.beta1
- Packaging of openvpn3-linux-5_beta release
- This release swaps out mbed TLS with OpenSSL
- Moving up to Python 3.6 dependency for RHEL 7

* Wed Mar 6 2019 David Sommerseth <davids@openvpn.net> - 4-0.beta2
- Added missing packaging of /var/lib/openvpn3/configs dir

* Fri Mar 1 2019 David Sommerseth <davids@openvpn.net> - 4-0.beta1
- Packaging of openvpn3-linux-4_beta release

* Thu Jan 31 2019 David Sommerseth <davids@openvpn.net> - 3-0.beta1
- Packaging of openvpn3-linux-3_beta release

* Wed Jan 30 2019 David Sommerseth <davids@openvpn.net> - 2-0.beta1
- Packaging of openvpn3-linux-2_beta release

* Sat Dec 8 2018 David Sommerseth <davids@openvpn.net> - 1-0.beta1
- First openvpn3-linux-1_beta release
